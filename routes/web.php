<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ChromaticController@index');

Route::post('/generate', 'ChromaticController@generate')->name('generate');

Route::get('/clear-db', 'ChromaticController@clearDB')->name('clear.db');

Route::get('/answer', 'ChromaticController@answer')->name('harmonious.chromatic.number');

Route::get('/test', function(){

	return view('test');

});

Route::get('/migrate-fresh', function() {
    $migrage = Artisan::call('migrate:fresh');  
    return '<h1>Migrate Fresh</h1>';
});
