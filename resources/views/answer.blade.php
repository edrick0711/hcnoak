
<div class="text-center">

	<span style="font-size: 20px"> <strong>({{ $answer->x }}, {{ $answer->y }}) </strong></span>
	
</div>
<hr>
<div class="text-center"  style="margin-top: 10px; margin-bottom: 10px">

	<button class="btn btn-float btn-sm" style="background-color: {{ $color }}"></button>
	
</div>

<hr>
<div class="text-center">

	<span style="font-size: 35px"><strong>{{ $answer->label }}</strong></span>	

</div>
