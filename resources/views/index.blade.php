  
<!DOCTYPE HTML>
<html>
<head>  

<body>

 
</body>


<script>



window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
    zoomEnabled: true,

	theme: "light2",
	title:{
		text: "Harmonious Chromatic Number of a Knight"
	},
	axisY:{
		includeZero: false,
		   minimum: 0,
		   interval: 1,


	},
	axisX:{
		includeZero: false,
		 minimum: 0,
		 interval: 1,
    viewportMaximum: 20
	},
	data: [{        
		type: "line", 
        // dataPoints : <?php echo json_encode($move); ?>         	
	}]
});
chart.render();


$('#generate').on('click', function() {

  var x_axis_size= $('#x_axis_size').val();

   $.ajax({
      url: "{{ route('generate') }}",
      data: { _token:"{{ csrf_token() }}", size: x_axis_size},
      type: 'POST',
      success: function(data) {

        $('#harmonious_chromatic_number').text(data.harmonious_chromatic_number);
              
          var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
              zoomEnabled: true,

            theme: "light2",
            title:{
              text: "Harmonious Chromatic Number of a Knight"
            },
            axisY:{
              includeZero: false,
                 minimum: 0,
                 interval: 1,


            },
            axisX:{
              includeZero: false,
               minimum: 0,
               interval: 1,
              viewportMaximum: 20,
              viewportMinimum: 5
            },
            data: [{        
              type: "line", 
                  dataPoints : data.move        
            }]
          });
          chart.render();


         

      }
  });


});



}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px;  margin: 0px auto;"></div>

<!-- <div class="chart-container" style="position: relative; height:40vh; width:80vw">
    <canvas id="chart"></canvas>
</div> -->

<div>
  <label>4 x</label>
  <input type="number" name="x_axis_size" id="x_axis_size">
  <button id="generate">Generate Graph</button>
  <label id="harmonious_chromatic_number"></label>
</div>


<script src="{{ asset('/js/canvasjs-2.3/canvasjs.min.js') }}"></script>
<script src="{{ asset('/js/jquery.min.js') }}"></script>

</body>
</html>

