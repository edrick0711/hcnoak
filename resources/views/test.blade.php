<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive admin dashboard and web application ui kit.">
    <meta name="keywords" content="dashboard, index, main">

    <title>HCNOAK &mdash; Main</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('/assets/css/core.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/app.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/css/style.min.css') }}" rel="stylesheet">

    <!-- Favicons -->
   <!--  <link rel="apple-touch-icon" href="../assets/img/apple-touch-icon.png">
    <link rel="icon" href="../assets/img/favicon.png">
 -->
    <!--  Open Graph Tags -->
   
  </head>

  <body>

    <!-- Preloader -->
    <div class="preloader">
      <div class="spinner-circle-material"></div>
    </div>

    <!-- Topbar -->
    <header class="topbar">
      <div class="topbar-left">

        <a class="topbar-btn d-none d-md-block" href="#" data-provide="fullscreen tooltip" title="Fullscreen">
          <i class="material-icons fullscreen-default">fullscreen</i>
          <i class="material-icons fullscreen-active">fullscreen_exit</i>
        </a>
        <div class="topbar-divider d-none d-md-block"></div>
        <span class="primary"><strong>Harmonious Chromatic Number</strong> of a <strong>Knight</strong></span>

      </div>

      <div class="topbar-right">


        <ul class="topbar-btns">
          <li class="dropdown">
            <span class="topbar-btn" data-toggle="dropdown"><img class="avatar" src="{{ asset('/img/fav.jpg') }}" alt="..."></span>
           
          </li>

         

         

        </ul>

      </div>
    </header>
    <!-- END Topbar -->


    <!-- Main container -->
    <main class="main-container">

      <div class="main-content">
        <div class="row">





          


          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"><strong>Graph</strong></h5>
                   <div class="flexbox">
                    <div class="">
                      <input class="form-control" type="number" name="x_axis_size" id="x_axis_size" placeholder="Enter the value N here...">
                    </div>

                    <button id="generate" class="btn btn-w-md btn-primary">Generate</button>
                    <button id="answer" class="btn btn-primary">Answer</button>
                  </div>
              </div>

             

              <div class="card-body">
                
<!--                 <canvas id="chart-js-2" height="130" data-provide="chartjs"></canvas>
 -->                <div id="chartContainer" style="height: 370px;  margin: 0px auto;"></div>
                    <div id="wrapper-load" style="height: 370px;  margin: 0px auto; display: none">
                   
                      <div class="text-center" style="align-items: center;position: absolute;top: 0;left: 0;right: 0;bottom: 0;display: flex;justify-content: center;align-items: center;">
                        

                       <div class="spinner-circle-material"></div>

                      </div>
                    
                    </div>

              </div>
            </div>
          </div>


      <!--     <div>
  <label>4 x</label>
  <input type="number" name="x_axis_size" id="x_axis_size">
  <button id="generate">Generate Graph</button>
  <label id="harmonious_chromatic_number"></label>
</div> -->






        </div>
      </div><!--/.main-content -->


      <!-- Footer -->
      <footer class="site-footer">
        <div class="row">
          <div class="col-md-6">
            <p class="text-center text-md-left">Copyright © 2019 <a href="#">HCNOAK</a>. All rights reserved.</p>
          </div>

         <!--  <div class="col-md-6">
            <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
              <li class="nav-item">
                <a class="nav-link" href="../help/articles.html">Documentation</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../help/faq.html">FAQ</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="https://themeforest.net/item/theadmin-responsive-bootstrap-4-admin-dashboard-webapp-template/20475359?license=regular&open_purchase_for_item_id=20475359&purchasable=source&ref=thethemeio">Purchase Now</a>
              </li>
            </ul>
          </div>
        </div> -->
      </footer>
      <!-- END Footer -->

    </main>
    <!-- END Main container -->

    <!-- Scripts -->
    <script src="{{ asset('/assets/js/core.min.js') }}"></script>
    <script src="{{ asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ asset('/assets/js/script.min.js') }}"></script>
    <script src="{{ asset('/js/canvasjs-2.3/canvasjs.min.js') }}"></script>


    <script>


      

      app.ready(function(){


         $('#generate').prop('disabled', true);
         $('#answer').prop('disabled', true);

         $('#x_axis_size').on('input', function() {
            
            $('#generate').prop('disabled', false);

            if($('#x_axis_size').val().length == 0) {
                $('#generate').prop('disabled', true);

            }
         });       

          $('#answer').on('click', function() {

             app.modaler({
              url: "{{ route('harmonious.chromatic.number') }}",
              type: 'center',
              title: 'Harmonious Chromatic Number',
              cancelVisible: false,

              onConfirm: function(modal) {
              },

              onCancel: function(modal) {
              }
            });
               
         });       


        var chart = new CanvasJS.Chart("chartContainer", {
          animationEnabled: true,
            zoomEnabled: true,

          theme: "light2",
         
          axisY:{
            includeZero: false,
               minimum: 0,
               interval: 1,


          },
          axisX:{
            includeZero: false,
             minimum: 0,
             interval: 1,
            viewportMaximum: 20
          },
          data: [{        
            type: "line", 
                dataPoints : <?php echo json_encode($move); ?>           
          }]
        });
        chart.render();


        $('#generate').on('click', function() {

          $('#wrapper-load').show();
          $('#chartContainer').hide();
          $('#answer').prop('disabled', true);

          var x_axis_size= $('#x_axis_size').val();

           $.ajax({
              url: "{{ route('generate') }}",
              data: { _token:"{{ csrf_token() }}", size: x_axis_size},
              type: 'POST',
              success: function(data) {

                $('#wrapper-load').hide();
                $('#chartContainer').show();
                $('#x_axis_size').val('');
                $('#generate').prop('disabled', true);
                $('#answer').prop('disabled', false);

                $('#harmonious_chromatic_number').text(data.harmonious_chromatic_number);
                      
                  var chart = new CanvasJS.Chart("chartContainer", {
                    animationEnabled: true,
                      zoomEnabled: true,

                    theme: "light2",
                   
                    axisY:{
                      includeZero: false,
                         minimum: 0,
                         interval: 1,


                    },
                    axisX:{
                      includeZero: false,
                       minimum: 0,
                       interval: 1,
                      viewportMaximum: 20
                    },
                    data: [{        
                      type: "line", 
                          dataPoints : data.move        
                    }]
                  });
                  chart.render();


                 

              }
          });


        });



      });




    </script>

  </body>
</html>
