<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{
    public function horseMove() {

        return $this->belongsTo(HorseMove::class);


    }
}
