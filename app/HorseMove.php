<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorseMove extends Model
{

	public function coordinates() {
        return $this->hasMany(Coordinate::class);
    }
   
}

