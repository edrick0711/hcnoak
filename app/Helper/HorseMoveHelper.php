<?php

namespace App\Helper;

class HorseMoveHelper
{

    public static function GetValidKnightSquares($x, $y, $size) {
    	
     	$intColVal = $x;
    	$intRowVal = $y;	
    	
    	$knightMoves = array();
    	array_push($knightMoves,array(1,2));
    	array_push($knightMoves,array(1,-2));
    	array_push($knightMoves,array(2,1));
    	array_push($knightMoves,array(2,-1));
    	array_push($knightMoves,array(-1,2));
    	array_push($knightMoves,array(-1,-2));
    	array_push($knightMoves,array(-2,1));
    	array_push($knightMoves,array(-2,-1));
    	
    	$validSquares = array();
    	
    	foreach($knightMoves as $move)
    	{
    		$move[0] = $move[0] + $intColVal;
    		$move[1] = $move[1] + $intRowVal;		     
    	
    		if ($move[0] > 0 && $move[0] <= $size && $move[1] > 0 && $move[1] <= 4)
    		{

    	 

    			array_push($validSquares,array('x' => $x , 'y' => $y, 'markerType' => 'circle', 'markerColor' => 'rgb(1,2,3)', 'markerSize' => 15, 'indexLabel' => null.'','is_belong'=> true));
    			array_push($validSquares,array('x' => $move[0] , 'y' => $move[1], 'markerType' => 'circle', 'markerColor' => 'rgb(1,2,3)', 'markerSize' => 15, 'indexLabel' => null.'', 'is_belong'=> true));
    			array_push($validSquares,array('x' => $x , 'y' => null, 'indexLabel' => null.'', 'is_belong'=> false));

    		}
    	}
    	
    	return $validSquares;	
    }


    public static function GetPossibleKnightMoves($x, $y, $size) {
        
        $intColVal = $x;
        $intRowVal = $y;    
        
        $knightMoves = array();
        array_push($knightMoves,array(1,2));
        array_push($knightMoves,array(1,-2));
        array_push($knightMoves,array(2,1));
        array_push($knightMoves,array(2,-1));
        array_push($knightMoves,array(-1,2));
        array_push($knightMoves,array(-1,-2));
        array_push($knightMoves,array(-2,1));
        array_push($knightMoves,array(-2,-1));
        
        $validSquares = array();
        array_push($validSquares,array('x' => $x , 'y' => $y));

        foreach($knightMoves as $move)
        {
            $move[0] = $move[0] + $intColVal;
            $move[1] = $move[1] + $intRowVal;            
        
            if ($move[0] > 0 && $move[0] <= $size && $move[1] > 0 && $move[1] <= 4)
            {
                // array_push($validSquares,array('x' => $x , 'y' => $y));
                array_push($validSquares,array('x' => $move[0] , 'y' => $move[1]));
            }
        }
        
        return $validSquares;   
    }
}
