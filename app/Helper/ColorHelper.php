<?php

namespace App\Helper;

class ColorHelper 
{


    public static  function RandomColorPart() {

        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);

    }


    public static function RandomColor() {

        return ColorHelper::RandomColorPart() . ColorHelper::RandomColorPart() . ColorHelper::RandomColorPart();

    }
   
}
